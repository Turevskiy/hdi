import express from 'express';
import { Application } from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import http from 'http';
import os from 'os';
import cookieParser from 'cookie-parser';
import Promise from 'bluebird';

import installValidator from './openapi';

import mongoose from "mongoose";


import l from './logger';

const app = express();

export default class ExpressServer {
  constructor() {

  // Connect to MongoDB
  const mongoUrl = process.env.MONGODB_URI;
  (<any>mongoose).Promise = Promise;

  mongoose.connect(mongoUrl, { useNewUrlParser: true} ).then(
      () => { 
        console.log("Connected to MongoDB")
      },
  ).catch(err => {
      console.log("MongoDB connection error. Please make sure MongoDB is running. " + err);
      process.exit();
  });

    const root = path.normalize(__dirname + '/../..');
    app.set('appPath', root + 'client');
    app.use(bodyParser.json({ limit: process.env.REQUEST_LIMIT || '100kb' }));
    app.use(bodyParser.urlencoded({ extended: true, limit: process.env.REQUEST_LIMIT || '100kb' }));
    app.use(cookieParser(process.env.SESSION_SECRET));
    app.use(express.static(`${root}/public`));
  }

  router(routes: (app: Application) => void): ExpressServer {
    installValidator(app, routes)
    return this;
  }

  listen(p: string | number = process.env.PORT): Application {
    const welcome = port => () => l.info(`up and running in ${process.env.NODE_ENV || 'development'} @: ${os.hostname() } on port: ${port}}`);
    http.createServer(app).listen(p, welcome(p));
    return app;
  }
}
