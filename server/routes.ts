import { Application } from 'express';
import patientsRouter from './api/controllers/patients/router'
import illnessesRouter from './api/controllers/illnesses/router'
import patientIllnessesRouter from './api/controllers/patientIllnesses/router'

export default function routes(app: Application): void {
  app.use('/api/v1/patients', patientsRouter);
  app.use('/api/v1/illnesses', illnessesRouter);
  app.use('/api/v1/patientIllnesses', patientIllnessesRouter);
};