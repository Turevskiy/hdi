import IllnessesService from '../../services/illnesses.service'
import { Request, Response } from 'express'

export class Controller {
  all(req: Request, res: Response): void {
    IllnessesService.all().then(r => res.json(r))
  }

  byIcd(req: Request, res: Response): void {
    IllnessesService.byIcd(req.params.icd).then(r => {
      if (r) res.json(r)
      else res.status(404).end()
    })
  }

  delete(req: Request, res: Response): void {
    IllnessesService.delete(req.params.icd).then(r => {
      if(r)
        res.send("OK")
      else
        res.status(404).send("Not found").end()
    },
    error => {
      res.status(404).send(error).end()
    })
  }

  create(req: Request, res: Response): void {
    IllnessesService.create(req.body.name, req.body.icd).then(
      r => res
        .status(201)
        .location(`/api/v1/illnesses/${req.body.icd}`)
        .json(r)
    , e => res
        .status(409)
        .send("Could not add an object: " + e.toString())
    )
  }
}
export default new Controller()
