import { Request, Response } from 'express'
import PatientsService from '../../services/patients.service'

export class Controller {
  all(req: Request, res: Response): void {
    PatientsService.all().then(r => res.json(r))
  }

  bySsn(req: Request, res: Response): void {
    PatientsService.bySsn(req.params.ssn).then(r => {
      if (r) res.json(r)
      else res.status(404).end()
    })
  }

  delete(req: Request, res: Response): void {
    PatientsService.delete(req.params.ssn).then(r => {
      if(r)
        res.send("OK")
      else
        res.status(404).send("Not found").end()
    },
    error => {
      res.status(404).send(error).end()
    })
  }

  create(req: Request, res: Response): void {
    PatientsService.create(req.body.name, req.body.ssn, req.body.birthday, req.body.illnesses).then(
      r => res
        .status(201)
        .location(`/api/v1/patients/${req.body.ssn}`)
        .json(r)
    , e => res
        .status(409)
        .send("Could not add an object: " + e.toString())
    )
  }
}
export default new Controller()
