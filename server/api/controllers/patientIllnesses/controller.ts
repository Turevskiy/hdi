import fetch from 'node-fetch'
import { Request, Response } from 'express'
import PatientsService from '../../services/patients.service'

const fetchJson = url => fetch(url).then(r => r.json())

export class Controller {
  all(req: Request, res: Response): void {
    Promise.all([
      fetchJson(process.env.PATIENTS_SERVICE_URL),
      fetchJson(process.env.ILLNESSES_SERVICE_URL)
    ])
    .then(jsons => 
      jsons[0].map(patient => {
        patient.illnesses = patient.illnesses.map(illnessIcd => 
        {
          let filteredIllnesses = jsons[1].filter(illness => illness.icd == illnessIcd)
          if(filteredIllnesses && filteredIllnesses[0]) {
            return filteredIllnesses[0]
          } else {
            return {icd: illnessIcd, error: "Not found"}
          }
        })
        return patient
      })
    )
    .then(json => res.json(json))
    .catch(err => res.status(404).send("Not found").end())
  }

  bySsn(req: Request, res: Response): void {
    Promise.all([
      fetchJson(`${process.env.PATIENTS_SERVICE_URL}/${req.params.ssn}`),
      fetchJson(process.env.ILLNESSES_SERVICE_URL)
    ])
    .then(jsons =>
    { 
      jsons[0].illnesses = jsons[0].illnesses.map(illnessIcd => 
      {
        let filteredIllnesses = jsons[1].filter(illness => illness.icd == illnessIcd)
        if(filteredIllnesses && filteredIllnesses[0]) {
          return filteredIllnesses[0]
        } else {
          return {icd: illnessIcd, error: "Not found"}
        }
      })
      return jsons[0]
    })
    .then(json => res.json(json))
    .catch(err => res.status(404).send("Not found").end())
  }
}
export default new Controller()
