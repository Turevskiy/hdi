import Promise, { reject } from 'bluebird'
import * as mongoose from 'mongoose'

import l from '../../common/logger'
 
export interface IPatient {
  ssn: String,
  name: String,
  birthday: Date,
  illnesses: Array<String>,
}

export class Patient implements IPatient {
  ssn: String
  name: String
  birthday: Date
  illnesses: Array<String>
  constructor(data: IPatient) {
    this.ssn = data.ssn
    this.name = data.name
    this.birthday = data.birthday
    this.illnesses = data.illnesses
    }
}

const patientSchema = new mongoose.Schema({
  ssn: {type: String, unique : true, required : true},
  name: {type: String, required : true},
  birthday: {type: String, required : true},
  illnesses: {type: [{type: String}], required : false}
})

export const PatientModel = mongoose.model<IPatient & mongoose.Document>('Patient', patientSchema)

export class PatientsService {
  all(): Promise<IPatient[]> {
    l.info('fetch all patients')

    return new Promise(function (resolve) { 
      PatientModel.find()
        .then(data => resolve(data.map(x=>new Patient(x))))
    })
  }

  bySsn(ssn: string): Promise<IPatient> {
    l.info(`fetch patient with ssn ${ssn}`)
    return new Promise(function (resolve) { 
      PatientModel.find({ ssn: ssn })
        .then(data => resolve(new Patient(data[0])))
        .catch(error => resolve(null))
    })
  }

  delete(ssn: string): Promise<boolean> {
    l.info(`delete patient with ssn ${ssn}`)
    return new Promise(function (resolve) { 
      PatientModel.deleteOne({ ssn: ssn })
        .then(
          data => 
          { 
            if(data.n) 
              resolve(true)
            else 
              resolve(false)
          },
          error => reject(error)
        )
    })
  }

  create(name: string, ssn: string, birthday: string, illnesses: Array<string>): Promise<IPatient> {
    l.info(`create patient with name ${name}, ssn ${ssn}, birthday ${birthday}, illnesses ${illnesses}`)
    return new Promise(function (resolve, reject) { 
      const parsedBirthday: Date = new Date(birthday)
      const patient: IPatient = {
        ssn,
        name,
        birthday: parsedBirthday,
        illnesses: illnesses
      }
      const createdPatient = new PatientModel(patient)
      createdPatient.save()
        .then(savedPatient => {
          resolve(new Patient(savedPatient))
        },
        e => {
          reject(e)
        })
    })
  }
}

export default new PatientsService()