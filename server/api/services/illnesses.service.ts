import Promise, { reject } from 'bluebird'
import l from '../../common/logger'

import * as mongoose from 'mongoose'
 
export interface IIllness {
  icd: String,
  name: String,
}

export class Illness implements IIllness {
  icd: String
  name: String
  constructor(data: IIllness) {
    this.icd = data.icd
    this.name = data.name
    }
}

const illnessSchema = new mongoose.Schema({
  icd: {type: String, unique : true, required : true},
  name: {type: String, required : true},
})

export const IllnessModel = mongoose.model<IIllness & mongoose.Document>('Illness', illnessSchema)

export class IllnessesService {
  all(): Promise<IIllness[]> {
    l.info('fetch all illnesses')

    return new Promise(function (resolve) { 
      IllnessModel.find()
        .then(data => resolve(data.map(x=>new Illness(x))))
    })
  }

  byIcd(icd: string): Promise<IIllness> {
    l.info(`fetch illness with icd ${icd}`)
    return new Promise(function (resolve) { 
      IllnessModel.find({ icd: icd })
        .then(data => resolve(new Illness(data[0])))
        .catch(error => resolve(null))
    })
  }

  delete(icd: string): Promise<boolean> {
    l.info(`delete illness with ssn ${icd}`)
    return new Promise(function (resolve) { 
      IllnessModel.deleteOne({ icd: icd })
        .then(
          data => 
          { 
            if(data.n) 
              resolve(true)
            else 
              resolve(false)
          },
          error => reject(error)
        )
    })
  }

  create(name: string, icd: string): Promise<IIllness> {
    l.info(`create illness with name ${name}, icd ${icd}`)
    return new Promise(function (resolve, reject) { 
      const illness: IIllness = {
        icd,
        name,
      }
      const createdIllness = new IllnessModel(illness)
      createdIllness.save()
        .then(savedIllness => {
          resolve(new Illness(savedIllness))
        },
        e => {
          reject(e)
        })
    })
  }
}

export default new IllnessesService()