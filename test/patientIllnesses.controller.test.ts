import 'mocha'
import { expect } from 'chai'
import request from 'supertest'
import Server from '../server'

function getRandomSsn(): string {
  return `TEST-${Math.round(Math.random()*1000000000)}`
}

function formatDate(d: Date) {
  var month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear()

  if (month.length < 2) month = '0' + month
  if (day.length < 2) day = '0' + day

  return [year, month, day].join('-')
}

describe('PatientIllnesses', () => {
  it('should get all patients and their illnesses', () =>
    request(Server)
      .get('/api/v1/patientIllnesses')
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body)
          .to.be.an('array')
      }))

  it('should get a patient and their illnesses by id', () => {
    let randomSsn = getRandomSsn()
    return request(Server)
    .post('/api/v1/patients')
    .send({ name: 'test', ssn: randomSsn, birthday: formatDate(new Date()), illnesses: ["S06.0x1A"]  })
    .then(() => request(Server).get(`/api/v1/patientIllnesses/${randomSsn}`)
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body)
          .to.be.an('object')
          .that.has.property('ssn')
          .equal(randomSsn)
        expect(r.body.illnesses)
          .to.be.an('array')
        expect(r.body.illnesses[0])
          .to.be.an('object')
          .that.has.property('icd')
          .equal('S06.0x1A')
      })
    )
    .finally(() => request(Server).delete(`/api/v1/patients/${randomSsn}`).send())
    })
})
