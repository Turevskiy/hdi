import 'mocha'
import { expect } from 'chai'
import request from 'supertest'
import Server from '../server'

function getRandomIcd(): string {
  return `TEST-${Math.round(Math.random()*100)}`
}

function formatDate(d: Date) {
  var month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear()

  if (month.length < 2) month = '0' + month
  if (day.length < 2) day = '0' + day

  return [year, month, day].join('-')
}

describe('Illnesses', () => {
  it('should get all illnesses', () =>
    request(Server)
      .get('/api/v1/illnesses')
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body)
          .to.be.an('array')
      }))

  it('should add a new illness', () => {
  let randomIcd = getRandomIcd()
  return request(Server)
      .post('/api/v1/illnesses')
      .send({ name: 'test', icd: randomIcd })
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body)
          .to.be.an('object')
          .that.has.property('icd')
          .equal(randomIcd)
      })
      .finally(() => request(Server).delete(`/api/v1/illnesses/${randomIcd}`).send())
    })

  it('should get an illness by id', () => {
    let randomIcd = getRandomIcd()
    return request(Server)
    .post('/api/v1/illnesses')
    .send({ name: 'test', icd: randomIcd })
    .then(() => request(Server).get(`/api/v1/illnesses/${randomIcd}`)
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body)
          .to.be.an('object')
          .that.has.property('icd')
          .equal(randomIcd)
      })
    )
    .finally(() => request(Server).delete(`/api/v1/illnesses/${randomIcd}`).send())
    })
})
