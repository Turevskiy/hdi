import 'mocha'
import { expect } from 'chai'
import request from 'supertest'
import Server from '../server'

function getRandomSsn(): string {
  return `TEST-${Math.round(Math.random()*1000000000)}`
}

function formatDate(d: Date) {
  var month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear()

  if (month.length < 2) month = '0' + month
  if (day.length < 2) day = '0' + day

  return [year, month, day].join('-')
}

describe('Patients', () => {
  it('should get all patients', () =>
    request(Server)
      .get('/api/v1/patients')
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body)
          .to.be.an('array')
      }))

  it('should add a new patient', () => {
  let randomSsn = getRandomSsn()
  return request(Server)
      .post('/api/v1/patients')
      .send({ name: 'test', ssn: randomSsn, birthday: formatDate(new Date()), illnesses: ["S06.0x1A"]  })
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body)
          .to.be.an('object')
          .that.has.property('ssn')
          .equal(randomSsn)
      })
      .finally(() => request(Server).delete(`/api/v1/patients/${randomSsn}`).send())
    })

  it('should get a patient by id', () => {
    let randomSsn = getRandomSsn()
    return request(Server)
    .post('/api/v1/patients')
    .send({ name: 'test', ssn: randomSsn, birthday: formatDate(new Date()), illnesses: ["S06.0x1A"]  })
    .then(() => request(Server).get(`/api/v1/patients/${randomSsn}`)
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body)
          .to.be.an('object')
          .that.has.property('ssn')
          .equal(randomSsn)
      })
    )
    .finally(() => request(Server).delete(`/api/v1/patients/${randomSsn}`).send())
    })
})
