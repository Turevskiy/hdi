# hdi

hdi test task: microservices example

Three microservices in a single application:
* Patients
* Services
* Patients with their illnesses combined from other services

Based on https://github.com/cdimascio/generator-express-no-stress-typescript

Please copy .env.TEMPLATE to .env and change connection settings if necessary,
see MONGODB_URI, PATIENTS_SERVICE_URL, and ILLNESSES_SERVICE_URL.

TODOs: 
* UPDATE is not implemented for both patients and illnesses
* No security considerations are implemented

## Quick Start

Get started developing...

```shell
# install deps
npm install

# run in development mode
npm run dev

# run tests
npm run test
```

---

## Install Dependencies

Install all package dependencies (one time operation)

```shell
npm install
```

## Run It
#### Run in *development* mode:
Runs the application is development mode. Should not be used in production

```shell
npm run dev
```

or debug it

```shell
npm run dev:debug
```

#### Run in *production* mode:

Compiles the application and starts it in production production mode.

```shell
npm run compile
npm start
```

## Test It

Run the Mocha unit tests

```shell
npm test
```

or debug them

```shell
npm run test:debug
```

## Try It
* Open you're browser to [http://localhost:3000](http://localhost:3000)
* Invoke the `/patients` endpoint 
  ```shell
  curl http://localhost:3000/api/v1/patients
  ```


## Debug It

#### Debug the server:

```
npm run dev:debug
```

#### Debug Tests

```
npm run test:debug
```
